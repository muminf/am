<?php
session_start();
//pre($_POST);
function format($number){
	return number_format(round($number,2),3);
}
function calc($credit_sum, $period, $period_type, $percent, $lang="ru"){
	$lang = (in_array($lang, array("en","tj")) ? $lang : "ru");
	$credit_sum = intval(str_replace(" ","",$credit_sum));
	$period = ($period_type == "Y" ? $period*12 : $period);
	$month_name = array(
		"ru" => array(1 => "Январь",2 => "Февраль",3 => "Март",4 => "Апрель",5 => "Май",6 => "Июнь",7 => "Июль",8 => "Август",9 => "Сентябрь",10 => "Октябрь",11 => "Ноябрь",12 => "Декабрь"),
		"tj" => array(1 => "Январ",2 => "Феврал",3 => "Март",4 => "Апрел",5 => "Май",6 => "Июн",7 => "Июл",8 => "Август",9 => "Сентябр",10 => "Октябр",11 => "Ноябр",12 => "Декабр"),
		"en" => array(1 => "January",2 => "Febrary",3 => "March",4 => "April",5 => "May",6 => "June",7 => "July",8 => "August",9 => "September",10 => "October",11 => "November",12 => "December")
	);
	
	$monthlyPercent = $percent/100/12;
	$monthlyPayment = $credit_sum*($monthlyPercent+($monthlyPercent/((pow((1+$monthlyPercent),$period))-1)));
	$overPayment = $monthlyPayment*$period-$credit_sum;
	$credit_sum_total = $credit_sum;
	$totalPaid = $monthlyPayment*$period;
	$percentsPayed = $totalPaid - $credit_sum_total;
	$schedule = '<div style="border-radius: 10px; overflow: hidden"><table class="schedule-table w-100"><tr><th class="text-center">№</th><th>Месяц</th><th class="d-sm-none">Платеж, <span class="text-muted">сом.</span></th><th class="d-none d-sm-table-cell">Сумма платежа, <span class="text-muted">сом.</span></th><th class="d-none d-sm-table-cell">Платеж по основному долгу, <span class="text-muted">сом.</span></th><th class="d-none d-sm-table-cell">Платеж по процентам, <span class="text-muted">сом.</span></th><th>Остаток долга, <span class="text-muted">сом.</span></th></tr>';
	$data_dates='[';
	$data_values1='[';
	$data_values2='[';
	$table_rows = "";
	$hider = '<tr><td class="text-center">...</td><td colspan="5" class="py-3"><a href="" onclick="$(this).closest(\'table\').find(\'tr\').show(); $(this).closest(\'tr\').remove(); sendHeight(); return false;" style="text-decoration: underline">Нажмите, чтобы показать все строки</a></td></tr>';
	for($i=1;$i<=$period;$i++){
		if($i<=12){
			$mi = $i;
		}else{
			if($i%12 == 0){
				$mi = 12;
			}else{
				$mi = $i%12;
			}
		}
		$y = ($i<=12 ? date("Y") : date("Y")+round($i/12));
		$curPercent = ($credit_sum * $monthlyPercent);
		$date = $month_name[$lang][$mi].' '.$y;
		$data_dates .= '"'.$date.'",';
		$data_values1 .= round(($monthlyPayment - $curPercent),2).',';
		$data_values2 .= round($curPercent,2).',';
		$credit_sum	-= ($monthlyPayment-$curPercent);
		$credit_sum = $credit_sum;
		$disp = "table-row";
		if($period>24 && $i>5){
			$schedule .= ($i==6 ? $hider : "");
			$disp = (($period-$i)>5 ? "none" : "table-row");
		}
		$schedule .= '<tr class="" style="display: '.$disp.'"><td class="text-nowrap text-center">'.$i.'</td><td>'.$date.'</td><td class="d-sm-none">'.format($monthlyPayment).'<div class="text-nowrap"><span class="text-muted">Долг:</span>'.format(($monthlyPayment-$curPercent)).'</div><div class="text-nowrap"><span class="text-muted">Проценты:</span>'.format($curPercent).'</div></td><td class="d-none d-sm-table-cell">'.format($monthlyPayment).'</td><td class="d-none d-sm-table-cell">'.format(($monthlyPayment-$curPercent)).'</td><td class="d-none d-sm-table-cell">'.format($curPercent).'</td><td>'.format($credit_sum).'</td></tr>';
		
	}
	$data_dates = substr($data_dates,0,-1)."]'";
	$data_values1 = substr($data_values1,0,-1)."]'";
	$data_values2 = substr($data_values2,0,-1)."]'";
	$schedule .= '<tr><td></td><td></td><td><span class="font-weight-bold">'.format($totalPaid).'</span><span class="text-muted">сомони</span><div class="mt-1 text-muted">(Выплачено всего)</div></td><td class="d-none d-sm-table-cell"><span class="font-weight-bold">'.format($credit_sum_total).'</span><span class="text-muted">сомони</span><div class="mt-1 text-muted">(Сумма выплаченного долга)</div></td><td class="d-none d-sm-table-cell"><span class="font-weight-bold">'.format($percentsPayed).'</span><span class="text-muted">сомони</span><div class="mt-1 text-muted">(Сумма выплаченных процентов)</div></td><td></td></tr></table></div>';
	
	$result = array(
		"monthlyPayment" => format($monthlyPayment),
		"overPayment" => format($overPayment),
		"totalPaid" => format($totalPaid),
		"chart_total" => '<canvas id="credit-total-chart" data-currency="сом." data-creditsum="'.round($credit_sum_total).'" data-overpayment="'.round($overPayment).'"></canvas>',
		"chart_payments" =>"<canvas id='credit-payments-chart' data-currency='сом.' data-dates='".$data_dates."' data-values1='".$data_values1."'  data-values2='".$data_values2."'></canvas>",
		"schedule" => $schedule,
		"ad" => "",
		"has_link" => null,
		"is_embed" => 0
	);
	
	return $result;
}
/*$result = calc(50000, 24, "M", 22);
pre($result);*/
if(isset($_POST)){
	$res = calc($_POST["credit_sum"], $_POST["period"], $_POST["period_type"], $_POST["percent"], $_POST["lang"]);
	echo json_encode($res);
}
function pre($data){
	echo "<pre>";
	var_dump($data);
	echo "</pre>";
}
?>	