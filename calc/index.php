<?php
  $lang = (isset($_GET["lang"]) && in_array($_GET["lang"], array("en","tj")) ? $_GET["lang"] : "ru");
  $labels = array(
    "ru" => array(
      "cred_calc" => "Кредитный калькулятор",
      "cred_sum" => "Сумма кредита",
      "cred_srok" => "Срок кредита",
      "cred_prc" => "Процентная ставка",
      "cred_yearly" => "годовых",
      "month" => "месяцев",
      "year" => "лет",
      "cred_calculate" => "РАССЧИТАТЬ",
      "somoni" => "сомони"
    ),
    "tj" => array(
      "cred_calc" => "Хисобкунаки карзи",
      "cred_sum" => "Маблаги карз",
      "cred_srok" => "Мухлати карз",
      "cred_prc" => "Фоизи карз",
      "cred_yearly" => "солона",
      "month" => "мох",
      "year" => "сол",
      "cred_calculate" => "ХИСОБКУНИ",
      "somoni" => "сомони"
    ),
    "en" => array(
      "cred_calc" => "Loan calculator",
      "cred_sum" => "Loan sum",
      "cred_srok" => "Loan period",
      "cred_prc" => "Loan percentage",
      "cred_yearly" => "yearly",
      "month" => "month",
      "year" => "year",
      "cred_calculate" => "CALCULATE",
      "somoni" => "somoni"
    )
  );
  $label = $labels[$lang];
?>
<html>

<head>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="css/libs.css?1">
  <link href="css/calc_common.css?36" rel="stylesheet" type="text/css">
  <link href="css/calc_calcus.css?3" rel="stylesheet" type="text/css">
  <link href="css/embed.css?9" rel="stylesheet" type="text/css">

  <meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1,user-scalable=no">
  <style type="text/css">
    /* Chart.js */
    
    @keyframes chartjs-render-animation {
      from {
        opacity: .99
      }
      to {
        opacity: 1
      }
    }
    
    .chartjs-render-monitor {
      animation: chartjs-render-animation 1ms
    }
    
    .chartjs-size-monitor,
    .chartjs-size-monitor-expand,
    .chartjs-size-monitor-shrink {
      position: absolute;
      direction: ltr;
      left: 0;
      top: 0;
      right: 0;
      bottom: 0;
      overflow: hidden;
      pointer-events: none;
      visibility: hidden;
      z-index: -1
    }
    
    .chartjs-size-monitor-expand>div {
      position: absolute;
      width: 1000000px;
      height: 1000000px;
      left: 0;
      top: 0
    }
    
    .chartjs-size-monitor-shrink>div {
      position: absolute;
      width: 200%;
      height: 200%;
      left: 0;
      top: 0
    }
  </style>
</head>

<body style="margin: 0">

  <style>
    @media all and (min-width: 481px) {
      .split .calc-fleft {
        width: 50%;
      }
      .split .calc-fright {
        margin-left: 56%;
      }
    }
    /*@media screen and (min-width: 768px) {
        .result-placeholder-schedule {
            padding: 0 0.5rem;
        }
    }*/
  </style>

  <style>
    .offers-frow {
      padding-bottom: 15px;
      margin-bottom: 1rem;
      border-bottom: 1px solid #dde5e7;
    }
	.block-heading {
		font-size: 20px;
		line-height: 24px;
		font-weight: 300;
		text-transform: uppercase;
		margin-bottom: 15px;
		color: #363636;
	}
	
  </style>
<div class="block-heading page-headers" style="margin:15px"><?=$label["cred_calc"];?></div>
  <form action="calc.php" method="post" class="calc-form" novalidate="novalidate">

    <input type="hidden" name="calculate" value="1">
    <input type="hidden" name="lang" value="<?=$lang;?>">
    




            <input type="hidden" name="type" value="2">
        <input type="hidden" name="cost" value="0">
        <input type="hidden" name="start_sum_type" value="1">
        <input type="hidden" name="start_sum" value="0">
    

    

    <div class="calc-frow type-x type-2" style="display:block">
        <div class="calc-fleft"><?=$label["cred_sum"];?></div>
        <div class="calc-fright">
            <div class="calc-input">
                <input required="" type="text" value="50000" name="credit_sum" inputmode="numeric" class="calc-inp" value="" style="width: 150px">
            </div>
            <div class="calc-input-desc"><?=$label["somoni"];?></div>
                    </div>
    </div>


    <div class="calc-frow">
        <div class="calc-fleft"><?=$label["cred_srok"];?></div>
        <div class="calc-fright">
            <div class="calc-input">
                <input required="" type="number" value="2" inputmode="numeric" name="period" min="1" max="50" class="period calc-inp" autocomplete="off" value="" data-max="50" style="width: 150px">
            </div>
            <div class="calc-input">
                <select name="period_type" class="period_type calc-inp">
                    <option value="Y" selected=""><?=$label["year"];?></option>
                    <option value="M"><?=$label["month"];?></option>
                </select>
            </div>
                    </div>
    </div>

            <div class="calc-frow">
            <div class="calc-fleft">
                <?=$label["cred_prc"];?>
            </div>
            <div class="calc-fright">
                <div class="calc-input">
                    <input required="" type="number" value="22" lang="en" step="0.01" min="0.01" max="99.99" name="percent" class="percent calc-inp" style="width: 150px" value="">
                </div>
                <div class="calc-input-desc">
                    % <?=$label["cred_yearly"];?>
                </div>
            </div>
        </div>
    


            <input type="hidden" name="payment_type" value="1">
    

    <div class="calc-frow button-row" style="border-bottom: 0px solid white;">
        <div class="calc-fright">
            <button type="submit" class="calc-submit"><?=$label["cred_calculate"];?></button>
            <div class="calc-count">Расчетов сегодня: <span class="today-count-placeholder">410</span></div>
        </div>

        <div class="calc-loading"></div>
    </div>



            <div class="calc-frow result-sep" style="display: "></div>
    



    <div class="calc-frow result-row offers-frow d-print-none" style="display: none">
        <div class="calc-fleft d-none d-sm-block">Предложения от банков</div>
        <div class="calc-fright">
            <div class="calc-result-value raw result-placeholder-offers"></div>
        </div>

    </div>



    <div class="row no-gutters split" id="calc-top">
        <div class="col-12 col-sm-6">
            <div class="calc-frow pr-0 result-row not-input" style="display: none">
                <div class="calc-fleft">Ежемесячный платеж</div>
                <div class="calc-fright">
                    <div class="calc-result-value result-placeholder-monthlyPayment">
                                            </div>
                    <span class="calc-text-desc">сомони</span>
                </div>
            </div>


            <div class="calc-frow pr-0 result-row not-input" style="display: none">
                <div class="calc-fleft">Переплата по кредиту</div>
                <div class="calc-fright">
                    <div class="calc-result-value result-placeholder-overPayment">
                                            </div>
                    <span class="calc-text-desc">сомони</span>
                </div>
            </div>

            <div class="calc-frow pr-0 result-row not-input" style="display: none">
                <div class="calc-fleft">Полная сумма с процентами</div>
                <div class="calc-fright">
                    <div class="calc-result-value result-placeholder-totalPaid">
                                            </div>
                    <span class="calc-text-desc">сомони</span>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-6">
            <div class="calc-frow result-row mt-3 mt-md-0 d-print-none" style="display: none">
                <div class="result-placeholder-chart_total" style="max-width: 320px; ">
                                    </div>
            </div>
        </div>
    </div>


    



    <div class="calc-frow result-row d-print-none" style="display: none">
        <div class="mb-2">График погашения</div>
        <div class="result-placeholder-chart_payments">
                    </div>
    </div>


    <div class="calc-frow result-row" style="display: none">
        <div class="result-placeholder-schedule">
                    </div>
    </div>

    
<!--<a id="hideCalc" href="#">Скрыть</a>-->
</form>

  <script>
    var isEmbed = true
	/*$(document).ready(function(
		console.log("hide");
		$("a#hideCalc").click(function(e){
			e.preventDefault();
			$("#calc_top,.result-row").hide();
		});
	));*/
  </script>

  <script src="js/libs.js"></script>
  <script src="js/calc.js?1"></script>
  <script src="js/calc_new.js"></script>
  <script src="js/embed_new.js?15"></script>
  <script type="text/javascript" src="js/Chart.bundle.min.js"></script>
  <script type="text/javascript" src="js/credit.js?7"></script>

</body>

</html>