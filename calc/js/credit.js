$('.calc-form').validate()

calcCreditSum()

$('input[name=cost]').autoNumeric('init', configForMoneyInt)
$('input[name=credit_sum]').autoNumeric('init', configForMoneyInt)
$('input[name=start_sum]').autoNumeric('init', configForMoneyInt)



function calcCreditSum() {
    var cost = $('input[name=cost]').val().replace(/ /g, '')
    if (cost == 0) {
        return false;
    }
    var start_sum_input = Number($('input[name=start_sum]').val().replace(/ /g, ''))
    var start_sum_type = $('select[name=start_sum_type]').val()
    var credit_sum = cost

    if (!cost) return false

    if (start_sum_input) {
        var start_sum
        var equiv
        switch (start_sum_type) {
            case '1':
                start_sum = start_sum_input
                equiv = '(' + (Math.round(start_sum / cost * 100 * 100) / 100) + '%)'
                break;
            case '2':
                start_sum = Math.round(cost * start_sum_input / 100)
                equiv = '(' + number_format(start_sum, 0, '.', ' ') + ' '+$('.start_sum_equiv').data('currency')+')'
                break;
        }
        credit_sum = cost - start_sum
        if (credit_sum < 0) credit_sum = 0
        $('.start_sum_equiv').text(equiv)
    } else {
        $('.start_sum_equiv').text('')
    }
    credit_sum = number_format(credit_sum, 0, '.', ' ')
    $('.credit_sum_value').text(credit_sum)
    $('input[name=credit_sum]').val(credit_sum)
}

$('input[name=cost], input[name=start_sum]').on('keyup change input', calcCreditSum)

$('select[name=start_sum_type]').on('change', function() {
    $('input[name=start_sum]').val('').focus()
    if ($(this).val() == 1) {
        $('input[name=start_sum]').autoNumeric('destroy')
        $('input[name=start_sum]').autoNumeric('init', configForMoneyInt)
    } else {
        $('input[name=start_sum]').autoNumeric('destroy')
        $('input[name=start_sum]').autoNumeric('init', configForPercentage)
    }
    calcCreditSum()
})

$('input[name=credit_sum]').on('change keyup input', function() {
    $('input[name=cost], input[name=start_sum]').val('')
    $('.credit_sum_value').text('0')
})


$('select[name=period_type]').change(function() {
    //$('input[name=period]').val('').focus()
    var maxYears = $('input[name=period]').data('max');
    if ($(this).val() == 'Y') {
        $('input[name=period]').attr('max', maxYears)
    } else {
        $('input[name=period]').attr('max', maxYears * 12)
    }
})


function onCalculate()
{

    Chart.defaults.global.defaultFontFamily = '-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,"Noto Sans",sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji"';
    Chart.defaults.global.defaultFontSize = 13;


    var ctx = $('#credit-payments-chart')

    var dates = ctx.data('dates')
    var values1 = ctx.data('values1')
    var values2 = ctx.data('values2')
    //var currency = ctx.data('currency')
    var currency = 'сом.'
	
    var width = document.documentElement.clientWidth

    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: dates,
            datasets: [{
                label: 'Основной долг',
                data: values1,
                backgroundColor: 'rgb(147, 112, 219, 0.7)',
            },{
                label: 'Проценты',
                data: values2,
                backgroundColor: 'rgb(255, 204, 51, 0.7)',
            }]
        },
        options: {
            aspectRatio: width > 600 ? 2.5 : 2,
            tooltips: {
                mode: 'index',
                callbacks: {
                    title: function(tooltipItem){
                        return this._data.labels[tooltipItem[0].index];
                    },
                    label: function(tooltipItems, data) {
                        return data.datasets[tooltipItems.datasetIndex].label +': ' + tooltipItems.yLabel.toString().replace(/(\d)(?=(\d{3})+$)/g, '$1 ') + ' ' + currency;
                    },
                    beforeBody: function (tooltipItems, data) {
                        var total = 0;
                        for (var i in tooltipItems) {
                            total+= tooltipItems[i].yLabel
                        }
                        return "Платеж: " + total.toString().replace(/(\d)(?=(\d{3})+$)/g, '$1 ') + ' ' + currency + "\n"
                    }
                }
            },
            scales: {
                xAxes: [{
                    stacked: true,
                    type: 'category',
                    gridLines: {
                        color: '#dde5e7'
                    },
                    ticks: {
                        display: width > 600,
                        maxRotation: 0,
                        autoSkip: false,
                        callback: function(value, index, values) {
                            if (values.length > 24) {
                                var year = value.substr(-4)
                                if (value.indexOf('Январь') == 0) {
                                    if (values.length > 180) {
                                        if (values.length > 240) {
                                            return year % 5 == 0 ? year : null
                                        } else {
                                            return year % 2 == 0 ? year : null
                                        }
                                    } else {
                                        return year
                                    }
                                } else {
                                    return null
                                }
                            } else {
                                // менее 24 мес.
                                if (values.length > 8) {
                                    return index % 2 == 0 ? value.split(' ')[0].substr(0,3) + ' ' + value.split(' ')[1] : null
                                } else {
                                    return value.split(' ')[0].substr(0,3) + ' ' + value.split(' ')[1]
                                }
                            }

                        }
                    }
                }],
                yAxes: [{
                    display: width > 600,
                    stacked: true,
                    ticks: {
                        beginAtZero: true,
                        display: false
                    },
                    gridLines: {
                        display: false,
                        color: '#dde5e7',
                        drawBorder: false
                    },
                    scaleLabel: {
                        display: true,
                        labelString: 'Ежемесячный платеж'
                    },
                }]
            },
            legend: {
                position: 'bottom',
            },

        }
    });



    var ctx1 = $('#credit-total-chart')

    var totalChart = new Chart(ctx1, {
        type: 'pie',
        data: {
            datasets: [{
                data: [ctx1.data('creditsum'), ctx1.data('overpayment')],
                backgroundColor: ['rgba(70, 130, 180, 0.7)', 'rgba(250, 128, 114, 1)'],
                borderWidth: 1
            }],
            labels: ['Сумма кредита', 'Сумма переплаты']
        },
        options: {
            showAllTooltips: true,
            legend: {
                position: 'right'
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItems, data) {
                        //return  data.labels[tooltipItems.index] +': '+data.datasets[0].data[tooltipItems.index].toString().replace(/(\d)(?=(\d{3})+$)/g, '$1 ') + ' ' + ctx1.data('currency') + '';
                        return  data.labels[tooltipItems.index] +': '+data.datasets[0].data[tooltipItems.index].toString().replace(/(\d)(?=(\d{3})+$)/g, '$1 ') + ' ' + 'сом.' + '';
                    },
                    afterLabel: function (tooltipItem, data) {
                        var dataset = data.datasets[tooltipItem.datasetIndex];
                        //calculate the total of this data set
                        var total = dataset.data.reduce(function(previousValue, currentValue, currentIndex, array) {
                            return previousValue + currentValue;
                        });
                        //get the current items value
                        var currentValue = dataset.data[tooltipItem.index];
                        //calculate the precentage based on the total and current item, also this does a rough rounding to give a whole number
                        var percentage = Math.floor(((currentValue/total) * 100)+0.5);

                        return "\n"+percentage+"% от всей суммы";
                    }
                }
            },
        }
    });

}

