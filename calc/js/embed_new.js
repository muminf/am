sendHeight = function(){
    var height = $('.calc-form').outerHeight()
    window.parent.postMessage({"height": height}, "*")
}

$(document).ready(function() {
    sendHeight()
})

$(window).on('resize', sendHeight)

$('.calc-toggle').click(function() {
    setTimeout(sendHeight, 1)
});

