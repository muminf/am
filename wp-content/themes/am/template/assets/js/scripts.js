var winWidth = jQuery(window).width();

jQuery(document).ready(function() {

	jQuery(".callback-form").submit(function() {
		var str = jQuery(this).serialize();

		jQuery.ajax({
			type: "POST",
			url: "/includes/contact.php",
			data: str,
			success: function() {
				jQuery(".callback-form").fadeOut(500);
				jQuery('#note').fadeIn(500);
			}
		});
		return false;
	});

});

jQuery(document).ready(function() {

	jQuery(".form-light").submit(function() {
		var str = jQuery(this).serialize();

		jQuery.ajax({
			type: "POST",
			url: "/includes/contact-bottom.php",
			data: str,
			success: function() {
				jQuery(".btn-form-bot").val('Отправлено!');
			}
		});
		return false;
	});

});

function barChart() {
	jQuery('.bar-chart').each(function(){
		jQuery(this).appear(function(){
			var percent = jQuery(this).data('percent');
			jQuery(this).find('.bar').animate({'width':percent});
			jQuery(this).find('.bar-percent').animate({ Counter: percent }, {
				duration: 800,
				easing: 'swing',
				step: function () {
					jQuery(this).text(Math.ceil(this.Counter)+('%'));
				}
			});
		});
	})
}

function accordion() {
	var trigger = jQuery('.accordion-toggle');
	jQuery('.accodion-wrapper').find('.accordion-section:first-child').find('.accordion-toggle').slideDown();
	trigger.click(function(){
		if(!jQuery(this).parents('.accordion-section').hasClass('accordion-current')){
			jQuery(this).parents('.accordion-section').siblings('.accordion-section').removeClass('accordion-current').find('.accordion-content').find('.accordion-toggle').slideUp();
			jQuery(this).parents('.accordion-header').siblings('.accordion-content').find('.accordion-toggle').slideDown().parents('.accordion-section').addClass('accordion-current');
		}
		else {
			jQuery(this).parents('.accordion-section').removeClass('accordion-current')
			jQuery(this).parents('.accordion-header').siblings('.accordion-content').find('.accordion-toggle').slideUp()
		}
	})
}

function stickyFooter() {
	var fHeight = jQuery('#footer').height();
	jQuery('.middle').css({'padding-bottom':fHeight});
	jQuery('#footer').css({'margin-top':-fHeight})
}
function equalHeight() {
	var winWidth = jQuery(window).width();
	if(winWidth > 991) {
		jQuery('.equal').each(function(){
		  	var currentTallest = 0,
		     currentRowStart = 0,
		     rowDivs = new Array(),
		     $el,
		     topPosition = 0;
			 jQuery(this).find('.block').each(function() {

			   $el = jQuery(this);
			   jQuery($el).height('auto')
			   topPostion = $el.position().top;

			   if (currentRowStart != topPostion) {
			     for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
			       rowDivs[currentDiv].height(currentTallest);
			     }
			     rowDivs.length = 0; // empty the array
			     currentRowStart = topPostion;
			     currentTallest = $el.height();
			     rowDivs.push($el);
			   } else {
			     rowDivs.push($el);
			     currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
			  }
			   for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
			     rowDivs[currentDiv].height(currentTallest);
			   }
			 });
		});
	}
	else {
		jQuery('.equal').each(function(){
			 $(this).find('.block').each(function() {
			   $el = $(this);
			   $($el).height('auto')
			});
		});
	}
}

// Mobile navigation
function mobileNav(){
    // Submenu effects
    jQuery('#nav').find('li').find('span').click(function(){
        if($(this).siblings('.sub-menu').is(':visible')){
          $(this).siblings('.sub-menu').slideUp();
        }
        else{
          $(this).siblings('.sub-menu').slideDown();
        }
    }); 
	jQuery('.mob-nav').click(function(){
		var winWidth = jQuery(window).width();
    	if((winWidth >= 600 && winWidth <= 991)){
	    	if(jQuery('body').hasClass('show-nav')){    		
		    	jQuery('body').removeClass('show-nav').animate({'padding-left':0});
		    	jQuery('#nav').animate({'left':'-100%'});
	    	}
	    	else{
		    	jQuery('body').addClass('show-nav').animate({'padding-left':'220px'});
		    	jQuery('#nav').animate({'left':0});
		    }
	    }
	    else if(winWidth <= 599){
	    	if(jQuery('body').hasClass('show-nav')){    		
		    	jQuery('body').removeClass('show-nav').animate({'padding-left':0});
		    	jQuery('#nav').slideUp();
	    	}
	    	else{
		    	jQuery('body').addClass('show-nav').animate({'padding-left':0});;
		    	jQuery('#nav').slideDown();
		    }
	    }
	});

}


// Custom scroll bar 
function customScrollBar() {
	    jQuery(".gallery-scroll").mCustomScrollbar({
        axis:'x',
        theme:"dark-thin",
        autoExpandScrollbar:true,
        advanced:{autoExpandHorizontalScroll:true}
    });
	jQuery('.carousel').flexslider({
	    animation: "slide",
	    animationLoop: false,
	    controlNav:false,
	    itemWidth: 200,
	    itemMargin: 5,
	    start: function(slider){
	      jQuery('body').removeClass('loading');
	    }
	});
	if(winWidth < 992){
	    jQuery(".nav-wrap").mCustomScrollbar({
	        axis:'y',
	        theme:"dark-thin",
	        autoExpandScrollbar:true,
	        advanced:{autoExpandHorizontalScroll:true}
	    });
	}
}

jQuery(document).ready(function(){
	var winWidth = jQuery(window).width(); // window width


    jQuery('.nav-wrap ul > li').mouseenter(function(){
    	jQuery(this).children('ul').stop().slideDown();
    });
    jQuery('.nav-wrap ul > li').mouseleave(function(){
    	jQuery(this).children('ul').stop().slideUp();
    });

	barChart(); // bar chart
	accordion(); // Accordion
	stickyFooter() // Sticky footer
    
    // Go to top 
    jQuery('.goto-top').click(function () {
      jQuery('body,html').animate({
        scrollTop: 0
      }, 800);
      return false;
    });

    // Mobile nav 
    mobileNav();

    // Animation
	//$("*[data-animation]").css({'visibility':'hidden'}); //hide all animated elemnts by default.
	// Animation Delay
	jQuery('*').each(function(){
		if(jQuery(this).attr('data-animation')) {
			jQuery(this).addClass('animate');
		}
		if(jQuery(this).attr('data-delay')) {
			var $delay = jQuery(this).attr('data-delay');
			jQuery(this).addClass($delay)
		}
		jQuery(this).appear(function() {
			if(jQuery(this).attr('data-animation')) {
				jQuery(this).css({'visibility':'visible'});
				var $animation = jQuery(this).attr('data-animation');
				jQuery(this).addClass('animated ' + $animation);

				if(jQuery(this).attr('data-delay')) {
					var $dataDelay = jQuery(this).attr('data-delay');
					var $delay = $dataDelay + 's';
					jQuery(this).css({'animation-delay':$delay, '-moz-animation-delay': $delay, '-webkit-animation-delay':$delay});
				}
				else {
					jQuery(this).css({'animation-delay':'0.5s', '-moz-animation-delay': '0.5s', '-webkit-animation-delay':'0.5s'});
				}
			}
		});
	});

    // Responsive tabs (Vertical)
    jQuery('.verticalTab').easyResponsiveTabs({
        type: 'vertical',
        width: 'auto',
        fit: true
    });

    // Responsive tabs (Horizontal)
    jQuery('.tab').easyResponsiveTabs({
        width: 'auto',
        fit: true
    });

    // Testimonial Slider
	if(jQuery('.testimonial-slider').length>0){
		  jQuery('.testimonial-slider').flexslider({
	    	directionNav:false,
	    });
	}
	 
	// Main slider
	if(jQuery('.wn_slider').length>0){
	    jQuery('.wn_slider').flexslider({
	    	// controlNav:false,
	    });
	}
	
	// Contents slider
	if(jQuery('.content_slider').length>0){
	    jQuery('.content_slider').flexslider({
	    	controlNav:false,
	    });
	}


	// Live counter
	jQuery('.live-count').each(function () {
	  jQuery(this).appear(function() {
	    var $this = jQuery(this);
	    jQuery({ Counter: 0 }).animate({ Counter: $this.text() }, {
	      duration: 3000,
	      easing: 'swing',
	      step: function () {
	        $this.text(Math.ceil(this.Counter));
	      }
	    });
	  });
	});

	jQuery('.block').each(function () {
		jQuery(this).appear(function(){
			equalHeight();
		});
	});

    // Pie Charts (Donut chart)
    var element = jQuery('.chart');
	jQuery(element).each(function() {
	    jQuery(this).appear(function(){
		    new EasyPieChart(this, {
		    	barColor: function(percent) {
			        var ctx = this.renderer.ctx();
			        var canvas = this.renderer.canvas();
			        var colors = jQuery('#nav').css("background-color");

			        function getHexColor(color){
    					color = color.replace(/\s/g,"");
    					var colorRGB = color.match(/^rgb\((\d{1,3}[%]?),(\d{1,3}[%]?),(\d{1,3}[%]?)\)$/i);
    					colorHEX = '';
    					for (var i=1;  i<=3; i++){
    					    colorHEX += Math.round((colorRGB[i][colorRGB[i].length-1]=="%"?2.55:1)*parseInt(colorRGB[i])).toString(16).replace(/^(.)$/,'0$1');
    					}
    					return "#" + colorHEX;
					}

			        var gradient = ctx.createLinearGradient(0,0,canvas.width,0);
			            gradient.addColorStop(0, getHexColor(colors));
			            gradient.addColorStop(0.8, getHexColor(colors));
			        return gradient;
		        }, 
		        trackColor: '#ededed',
		        scaleColor: false,
		        lineWidth: 8,
		        lineCap: 'butt',
		        size: 130,
		        onStep: function(from, to, percent) {
		            this.el.children[0].innerHTML = Math.round(percent) + ('%');
		        }
		    });
		});
	})

	// Contact fom 7 - Submit button 
	jQuery('.wpcf7-form input[type="submit"]').wrap( "<div class='btn-send'></div>" );
	jQuery("<i class='fa fa-paper-plane'></i>").insertBefore('.wpcf7-form input[type="submit"]');

});


jQuery(window).load(function() {
	var winWidth = jQuery(window).width();
	customScrollBar();
	equalHeight();
});

// Functions on window resize
jQuery(window).resize(function(){
	var winWidth = jQuery(window).width();
	equalHeight();
	if(winWidth >= 600){
		jQuery('#nav').show();
		if(jQuery('body').hasClass('show-nav')){
			jQuery('body').animate({'padding-left':'220px'});
			jQuery('#nav').animate({'left':'0'});
		}
		else {
			jQuery('#nav').animate({'left':'-100%'})
		}
	}
	else {
		jQuery('body').css({'padding-left':'0'});
		if(jQuery('body').hasClass('show-nav')){
			jQuery('#nav').show();
		}
		else {
			jQuery('#nav').hide();
		}
	}
});

jQuery(document).ready(function(){
	jQuery('.price-box a[href="#myModal"]').click(function(){
		jQuery('#my-modal').fadeIn();
	})

	jQuery('#my-modal .fa-remove').click(function(){
		jQuery('#my-modal').fadeOut();
	})
});

