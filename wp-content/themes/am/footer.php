<!-- Footer -->
<footer id="footer" style="margin-top: -188px;">
    <div class="container">

      <div class="custom">
        <ul class="social-icons animate animated fadeInDown" data-animation="fadeInDown" style="visibility: visible; animation-delay: 0.5s;">
          <li><a href="https://www.facebook.com/AziziMoliya/about/"><i class="fa fa-facebook">&nbsp;</i> <span class="tt">Facebook</span></a></li>
          <li><a href="http://businesses.html6.us/index.php#"><i class="fa fa-youtube">&nbsp;</i> <span class="tt">Youtube</span></a></li>
        </ul>
      </div>

      <img style="width:70px" src="<?php echo get_template_directory_uri()?>/assets/image/logo.png" data-animation="fadeIn" class="animate animated fadeIn" style="visibility: visible; animation-delay: 0.5s;">
      <p style="text-align: center">© 2019  | МДО "Азизи молия"</p>
      <div class="goto-top animate animated fadeInUp" data-animation="fadeInUp" style="visibility: visible; animation-delay: 0.5s;">
      </div>
    </div>
  </footer>
  <!-- Footer -->
<!--
  <div class="sticky-left" style="left: -305px;">
    <div class="config-btn closed"><span class="fa fa-cogs"></span></div>
    <div class="mod-content">
      <div class="moduletable">
        <h3>Выбор стиля</h3>

        <div class="custom">
          <div class="wrap-colors">
            <div class="theme-style gold-theme">Выбор</div>
            <div class="theme-style red-theme">Выбор</div>
            <div class="theme-style blue-theme">Выбор</div>
          </div>
          <div class="wrap-bg"><img src="<?php echo get_template_directory_uri()?>/assets/image/bg-1.jpg" alt=""> <img src="<?php echo get_template_directory_uri()?>/assets/image/bg-2.jpg" alt=""> <img src="<?php echo get_template_directory_uri()?>/assets/image/bg-3.jpg" alt=""></div>
        </div>
      </div>

    </div>
  </div>
 -->
  <div id="my-modal" style="display:none;">
    <div class="modal-block">

      <div class="custom">
        <p><span class="fa fa-remove">&nbsp;</span></p>
        <div id="note">Ваше сообщение принято. Мы свяжемся с Вами.</div>
        <form class="callback-form" action="http://businesses.html6.us/index.php#" method="post">
          <input name="name" required="required" type="text" placeholder="Имя">
          <input name="email" required="required" type="text" placeholder="Почта">
          <select name="tarif" required="required">
            <option value="Стандартный">Стандартный</option>
            <option value="Стартовый">Стартовый</option>
            <option value="Максимальный">Максимальный</option>
          </select>
          <input name="subject" type="text" placeholder="Тема">
          <textarea name="message" required="required" placeholder="Ваше сообщение"></textarea> <span class="btn-send"><i class="fa fa-paper-plane">&nbsp;</i><input name="submit" type="submit" value="Отправить"></span></form>
      </div>

    </div>
  </div>

  <noindex>
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript">
      (function(d, w, c) {
        (w[c] = w[c] || []).push(function() {
          try {
            w.yaCounter37968170 = new Ya.Metrika({
              id: 37968170,
              clickmap: true,
              trackLinks: true,
              accurateTrackBounce: true
            });
          } catch (e) {}
        });
        var n = d.getElementsByTagName("script")[0],
          s = d.createElement("script"),
          f = function() {
            n.parentNode.insertBefore(s, n);
          };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";
        if (w.opera == "[object Opera]") {
          d.addEventListener("DOMContentLoaded", f, false);
        } else {
          f();
        }
      })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript>
      <div><img src="https://mc.yandex.ru/watch/37968170" style="position:absolute; left:-9999px;" alt="" /></div>
    </noscript>
    <!-- /Yandex.Metrika counter -->

  </noindex>

</body>

</html>