<!-- Icon holder -->
<div class="row equal">
  <?php
    $args = [
        'post_type'      => 'post',
        'numberposts' => 4,
        'taxonomy'  => 'adv_block'
    ];
    $posts = get_posts($args);
    foreach($posts as $post): setup_postdata($post);
    $text = explode(";",$post->post_content);
    $icon = $text[0];
    $link = $text[1];
  ?>
    <div class="col-md-3">
      <div class="block text-center animate animated fadeIn" data-animation="fadeIn" style="height: 230px; visibility: visible; animation-delay: 0.5s;">
        <div class="block-inner">

          <div class="custom3">
            <div class="icon-top icon-x4">
              <div class="icon-holder anim1"><i class="<?php echo $icon;?>">&nbsp;</i></div>
              <div class="block-heading"><?php echo the_title();?></div>
              <a class="btn" href="<?=get_home_url();?>/<?php echo $link;?>">Подробнее</a></div>
          </div>
        </div>
      </div>
    </div>
  <?php endforeach;?>
</div>