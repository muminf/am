<div class="block-heading page-headers"><?=$label["currency_rate"];?></div>
  <div class="only_recent">
    <?php 
      $rates = get_rate();
      $rates_nbt = get_rate($nbt=true);
      $month = intval(date("m"));
    ?>
    <table>
      <tbody>
        <tr style="border-top: none !important;">
          <td colspan="3" style="color: #f7c272;"><?=date("d")." ".$label["month_name"][$month]." ".date("Y");?></td>
          <td><span style="color: #f7c272;"><?=$label["nbt"];?>(TJS)</span></td>
        </tr>
        <?php foreach($rates_nbt as $rate):?>
          <tr>
            <td><img height="20px" src="<?php echo get_template_directory_uri()?>/assets/image/rate/<?=$rate->char_code;?>.png" style="title=" usd '=""></td>
            <td style="color: #f7c272;"><?=$rate->char_code;?><img height="10px" width="10px" src="<?php echo get_template_directory_uri()?>/assets/image/rate/up.png" style="margin-top: 8px; margin-left:5px;" title="0.0036 TJS"></td>
            <td><?=$label["currency"][$rate->id];?></td>
            <td><?=$rate->sell;?></td>
          </tr>
        <?php endforeach;?>

        <tr>
          <td colspan="2" class="curs_header">Азизи Молия</td>
          <td class="curs_header"><?=$label["buy"];?> <span style="color: #000; font-size: 10px"> ↑↓</span></td>
          <td class="curs_header" style="min-width: 76px;"><?=$label["sell"];?><span style="color: #000; font-size: 10px"> ↑↓</span></td>
        </tr>
        <?php foreach($rates as $rate):?>
          <tr>
            <td><img height="20px" src="<?php echo get_template_directory_uri()?>/assets/image/rate/<?=$rate->char_code;?>.png" style="title=" rub'=""></td>
            <td style="color:#f7c272; min-width: 41px;"><?=$rate->char_code;?></td>
            <td class="curs_value"><?=$rate->buy;?></td>
            <td class="curs_value"><?=$rate->sell;?></td>
          </tr>
        <?php endforeach;?>
      </tbody>
    </table>
  </div>