<div class="row  section">
   <div class="col-md-12" style="margin-bottom:20px;">
      <div class="content" style="padding:35px" itemscope="" itemtype="https://schema.org/Person">
        <div class="page-header">
           <h2>
              <span class="contact-name" itemprop="name"><?php the_title();?></span>
           </h2>
        </div>
        <div class="post-content">
         <?php
          $page = get_post(); 
          $menu = "";
          $content = "";
          $args = [
           'post_type'      => 'post',
           'taxonomy'  => $taxonomy
          ];
          $posts = get_posts($args);
          foreach($posts as $post): 
         //setup_postdata($post);
            $menu .= '<li class="resp-tab-item resp-tab" aria-controls="tab_item-0" role="tab"><i class="fa fa-star"></i>'.$post->post_title.'</li>';  
            $content .= '<h2 class="resp-accordion" role="tab" aria-controls="tab_item-1"><span class="resp-arrow"></span><i class="fa fa-unlock"></i>'.$post->post_title.' </h2>
              <div class="resp-tab-content" aria-labelledby="tab_item-1">
                <p>'.$post->post_content.'</p>
              </div>';  
            
         endforeach;
        ?>
         
        <div>
          <div class="tab" style="display: block; width: 100%; margin: 0px;">
            <ul class="resp-tabs-list">
              <?=$menu;?>
            </ul>
            <div class="resp-tabs-container">
              <?=$content;?>
            </div>
          </div>
          <span class="clearfix"></span>
        </div>
        <br><br><br>
        <?php  echo $page->post_content;?>
        </div>
    </div>
  </div>
</div>


    