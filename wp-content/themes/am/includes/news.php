<span class="separator"></span>
<div class="custom6" style="padding-left:5px;">
  <?php
      $args = array(
          'numberposts' => 4,
          'suppress_filters' => true,
          'taxonomy' => 'news'
      );
      $posts = get_posts($args);
      $i = 1;
      foreach($posts as $post){ setup_postdata($post);
  ?>
<div class="icon-left icon-x2" style="padding-left:0px;padding-top:0px">
  <a href="<?php the_permalink();?>" style="color:#ffffff">
    <div class="section-heading">
      <img width="90px" src="<?php echo get_the_post_thumbnail_url(null,'thumbnail'); ?>">
      <?php the_title(); ?>
      <div class="section-heading" style="font-size:0.7em;display: inline;"><?php the_date(); ?></div>
    </div>   
  </a>
</div>
<?php }?>