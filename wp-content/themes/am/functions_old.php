<?php

add_action('wp_enqueue_scripts', 'style_theme');
add_action('wp_enqueue_scripts', 'scripts_theme');
add_action('after_setup_theme', 'main_menu');

function style_theme() {
    $uri = get_template_directory_uri();
    wp_enqueue_style('style', get_stylesheet_uri());
    wp_enqueue_style('simple-line-icons.min.css', $uri . '/assets/css/simple-line-icons.min.css');
    wp_enqueue_style('k2.css', $uri . '/assets/css/k2.css');
    wp_enqueue_style('bootstrap.css', $uri . '/assets/css/bootstrap.css');
    wp_enqueue_style('animate.css', $uri . '/assets/css/animate.css');
    wp_enqueue_style('font-awesome.min.css', $uri . '/assets/css/font-awesome.min.css');
    wp_enqueue_style('style.css', $uri . '/assets/css/style.css');
    wp_enqueue_style('theme.css', $uri . '/assets/css/theme.css');
    wp_enqueue_style('css.css', $uri . '/assets/css/css.css');
}

function scripts_theme() {
    $uri = get_template_directory_uri();
    wp_enqueue_script('watch.js', $uri . '/assets/js/watch.js');
    wp_enqueue_script('k2.frontend.js', $uri . '/assets/js/k2.frontend.js');
    wp_enqueue_script('jquery.min.js', $uri . '/assets/js/jquery.min.js');
    wp_enqueue_script('jquery-noconflict.js', $uri . '/assets/js/jquery-noconflict.js');
    wp_enqueue_script('jquery-migrate.min.js', $uri . '/assets/js/jquery-migrate.min.js');
    wp_enqueue_script('caption.js', $uri . '/assets/js/caption.js');
    wp_enqueue_script('newCapt.js', $uri . '/assets/js/newCapt.js');
   
    wp_enqueue_script('modernizr.js', $uri . '/assets/js/modernizr.js');
    wp_enqueue_script('jquery-1.11.1.min.js', $uri . '/assets/js/jquery-1.11.1.min.js');
    wp_enqueue_script('jquery.flexslider.js', $uri . '/assets/js/jquery.flexslider.js');
    wp_enqueue_script('js(1).js', $uri . '/assets/js/js(1)');
    wp_enqueue_script('plugins.js', $uri . '/assets/js/plugins.js');
    wp_enqueue_script('scripts.js', $uri . '/assets/js/scripts.js');
    wp_enqueue_script('common.js', $uri . '/assets/js/common.js');
    wp_enqueue_script('util.js', $uri . '/assets/js/util.js');


    wp_enqueue_script('init_embed.js', $uri . '/assets/js/init_embed.js');
    wp_enqueue_script('map.js', $uri . '/assets/js/map.js');
    wp_enqueue_script('overlay.js', $uri . '/assets/js/overlay.js');
}

function main_menu() {
    register_nav_menu('main', 'Главное меню');
}

function get_rate($nbt=false) {
    global $wpdb;
    $sql = "SELECT cr.char_code AS char_code, cr.name AS NAME, cr.nominal AS nominal, rt.buy AS buy, rt.buy_fiz AS buy_fiz, rt.sell AS sell, rt.sell_fiz AS sell_fiz, rt.datetime AS DATE 
            FROM currency AS cr, currency_rate".($nbt>0 ? "_nbt" : "")." AS rt 
            WHERE rt.currency IN(2,3,4) AND rt.currency=cr.id GROUP BY rt.`currency`,rt.datetime ORDER BY rt.id DESC,DATETIME DESC LIMIT 3";
    $result = $wpdb->get_results($sql);
    return $result;   
}

$key = "qwerty";
function getToken($data){
    global $key;
    return sha1(md5(json_encode($data)).$key);
}

function load_rate($data) {
    global $wpdb;
    $rates = $data["rate"];
    $format = array('%d','%f','%f','%s');
    foreach($rates as $k=>$v){
        $rates[$k]["datetime"] = date("Y-m-d H:i:s");
        $wpdb->insert("currency_rate",$rates[$k],$format);
    }
}

function get_submenu($css_class="") {
    $result = array();
    $result["list"] = "";
    $id = "";
    if ( $menu_items = wp_get_nav_menu_items(2) ) {  //var_dump($menu_items);
        foreach ( $menu_items as $menu_item ) {
            if($menu_item->object_id == get_queried_object_id()){
                $result["title"] = $menu_item->title;
                $id = $menu_item->post_name;
            }
           
            if($menu_item->menu_item_parent ==  $id){
                $current = ( $menu_item->object_id == get_queried_object_id() ) ? $css_class : '';
                $result["list"] .= '<li class="' . $current . '"><a href="' . $menu_item->url . '">' . $menu_item->title . '</a></li>';
            }
        }
    }
    return $result;
}

add_shortcode('cred_calc', 'add_cred_calc');

function add_cred_calc() {
    return '<div class="block-heading page-headers">Кредитный калькулятор</div>
    <iframe id="calcus-iframe-credit" width="100%" height="300" frameborder="0" style="width:1px; min-width: 100%;" scrolling="no" src="https://calcus.ru/kreditnyj-kalkulyator?embed=1&payment_type=1&currency_text=%D1%81%D0%BE%D0%BC%D0%BE%D0%BD%D0%B8"></iframe>
        
        <script>
            window.addEventListener(\'message\', function(event) {
                if(height = event.data[\'height\']) {
                    document.getElementById(\'calcus-iframe-credit\').style.height =  height + \'px\'
                }
            })
        </script>';
}