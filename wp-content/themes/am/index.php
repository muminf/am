<?php get_header(); 
  $pagename = get_query_var('pagename'); 
  
  $json = file_get_contents('php://input');
  $my_post = json_encode($json);
  file_put_contents("1.txt",$my_post);

  if($pagename == "loadrate"){
    $data = $_POST;
    if(checkToken($data["token"],$data["rate"])){
      load_rate($data["rate"]);
    }
  }
  if(isset($_POST["phone"])){
    $arr = array(
      "name"  => $_POST["name"],
      "phone"  => $_POST["phone"],
      "message" => $_POST["message"]
    );
    $chat_id = "384384648";
    $token = "946901155:AAEG3_vRIuIea9aqq8ZPFRKSlTevN4CP3B4";
    $tlg = send_telegram($chat_id,$token,$_POST);
    unset($_POST);
  }
?>
      <!-- Middle Area -->
      <div class="middle" style="padding-bottom: 188px;">
        <?php get_template_part("includes/adv_blocks"); ?>

        <div class="row equal">
          <!-- Icon box -->
          <div class="col-md-6">
            <div class="block bg-1 animate animated fadeInLeft" data-animation="fadeInLeft" style="height: 400px; visibility: visible; animation-delay: 0.5s;">
              <div class="block-inner offer-block" style="min-height:100%;/*background-color: #f7c272;*/">
                <div class="block-heading page-headers"><?=$label["news"];?></div>
                  <?php get_template_part("includes/news"); ?>
                </div>
              </div>
            </div>
          </div>
          <!-- Tab -->

          <div class="col-md-6">
            <div class="block animate animated fadeInRight" data-animation="fadeInRight" style="height: 400px; visibility: visible; animation-delay: 0.5s;">
              <div class="block-inner">
                <div class="block-heading page-headers"><?=$label["currency_rate"];?></div>
                  <div class="only_recent">
                    <?php 
                      $rates = get_rate();
                      $rates_nbt = get_rate($nbt=true);
                      $month = intval(date("m"));
                    ?>
                    <table>
                      <tbody>
                        <tr style="border-top: none !important;">
                          <td colspan="3" style="color: #f7c272;"><?=date("d")." ".$label["month_name"][$month]." ".date("Y");?></td>
                          <td><span style="color: #f7c272;"><?=$label["nbt"];?>(TJS)</span></td>
                        </tr>
                        <?php foreach($rates_nbt as $rate):?>
                          <tr>
                            <td><img height="20px" src="<?php echo get_template_directory_uri()?>/assets/image/rate/<?=$rate->char_code;?>.png" style="title=" usd '=""></td>
                            <td style="color: #f7c272;"><?=$rate->char_code;?><img height="10px" width="10px" src="<?php echo get_template_directory_uri()?>/assets/image/rate/up.png" style="margin-top: 8px; margin-left:5px;" title="0.0036 TJS"></td>
                            <td><?=$label["currency"][$rate->id];?></td>
                            <td><?=$rate->sell;?></td>
                          </tr>
                        <?php endforeach;?>

                        <tr>
                          <td colspan="2" class="curs_header">Азизи Молия</td>
                          <td class="curs_header"><?=$label["buy"];?> <span style="color: #000; font-size: 10px"> ↑↓</span></td>
                          <td class="curs_header" style="min-width: 76px;"><?=$label["sell"];?><span style="color: #000; font-size: 10px"> ↑↓</span></td>
                        </tr>
                        <?php foreach($rates as $rate):?>
                          <tr>
                            <td><img height="20px" src="<?php echo get_template_directory_uri()?>/assets/image/rate/<?=$rate->char_code;?>.png" style="title=" rub'=""></td>
                            <td style="color:#f7c272; min-width: 41px;"><?=$rate->char_code;?></td>
                            <td class="curs_value"><?=$rate->buy;?></td>
                            <td class="curs_value"><?=$rate->sell;?></td>
                          </tr>
                        <?php endforeach;?>
                      </tbody>
                    </table>
                  </div>

                <span class="clearfix"></span>
              </div>
            </div>
          </div>

        </div>

        <!-- Piechart and bar -->
        <div class="row equal">

          <div class="col-md-12">
                      
            <?php echo do_shortcode("[cred_calc]"); ?>

          </div> 
        </div>

        <!-- Tab -->
        <div class="row  section">

          <div class="col-md-12">
            <div class="block animate animated fadeInRight" data-animation="fadeInRight" style="visibility: visible; animation-delay: 0.5s;">
              <div class="block-inner">
                <div class="block-heading 12"><?=$label["about"];?></div>

                <div class="tab" style="display: block; width: 100%; margin: 0px;">
                  <ul class="resp-tabs-list">
                    <li class="resp-tab-item resp-tab-active" aria-controls="tab_item-0" role="tab"><i class="fa fa-star"></i> История</li>
                    <li class="resp-tab-item" aria-controls="tab_item-1" role="tab"><i class="fa fa-unlock"></i> Миссии </li>
                    <li class="resp-tab-item" aria-controls="tab_item-2" role="tab"><i class="fa fa-cogs"></i> Цели</li>
                    <li class="resp-tab-item" aria-controls="tab_item-3" role="tab"><i class="fa fa-plane"></i> Задачи</li>
                    <li class="resp-tab-item" aria-controls="tab_item-4" role="tab"><i class="fa fa-heart"></i> Информация</li>
                  </ul>
                  <div class="resp-tabs-container">
                    <h2 class="resp-accordion resp-tab-active" role="tab" aria-controls="tab_item-0"><span class="resp-arrow"></span><i class="fa fa-star"></i> История</h2>
                    <div class="resp-tab-content resp-tab-content-active" aria-labelledby="tab_item-0" style="display:block">
                      <p>ООО  МДО«Азизи- Инвест» является объеденным открытным обществом, созданным в Республике Таджикистан в соответствии с учредительным договором от  августа  2006 года.Согласно решения Наблюдательного Совета в 2011 году организация сменила Учредителя.
                     С целью соблюдение  требование Закона Республики Таджикистан «О государственном языке», и в соответствии с требованием Национального Банка Республики Таджикистан, на основании решения внеочередного собрания акционеров ООО МДО «Азизи-Инвест» был переименован в ООО МДО «Азизи-Молия».
                    Лицензию на осуществление банковской деятельности ООО  МДО«Азизи- Молия»  получил от Национального Банка Таджикистана  16 ноября 2011 года. 
                    </p>
                    </div>
                    <h2 class="resp-accordion" role="tab" aria-controls="tab_item-1"><span class="resp-arrow"></span><i class="fa fa-unlock"></i> Миссии </h2>
                    <div class="resp-tab-content" aria-labelledby="tab_item-1">
                      <p>Каждая сфера рынка существенно индуцирует межличностный продукт. BTL наиболее полно оправдывает маркетинг. Инструмент маркетинга неверно изменяет культурный портрет потребителя. В соответствии с законом Ципфа, ретроконверсия национального наследия выражена наиболее полно. Опрос усиливает продвигаемый целевой сегмент рынка, используя опыт предыдущих кампаний.</p>
                    </div>
                    <h2 class="resp-accordion" role="tab" aria-controls="tab_item-2"><span class="resp-arrow"></span><i class="fa fa-cogs"></i> Цели</h2>
                    <div class="resp-tab-content" aria-labelledby="tab_item-2">
                      <p>Концепция развития транслирует социометрический конкурент. Отсюда естественно следует, что анализ зарубежного опыта версифицирован. Партисипативное планирование поддерживает общественный продуктовый ассортимент. Медиа переворачивает принцип восприятия, учитывая результат предыдущих медиа-кампаний.</p>
                    </div>
                    <h2 class="resp-accordion" role="tab" aria-controls="tab_item-3"><span class="resp-arrow"></span><i class="fa fa-plane"></i> Задачи</h2>
                    <div class="resp-tab-content" aria-labelledby="tab_item-3">
                      <p>Каждая сфера рынка существенно индуцирует межличностный продукт. BTL наиболее полно оправдывает маркетинг. Инструмент маркетинга неверно изменяет культурный портрет потребителя. В соответствии с законом Ципфа, ретроконверсия национального наследия выражена наиболее полно. Опрос усиливает продвигаемый целевой сегмент рынка, используя опыт предыдущих кампаний.</p>
                    </div>
                    <h2 class="resp-accordion" role="tab" aria-controls="tab_item-4"><span class="resp-arrow"></span><i class="fa fa-heart"></i> Информация</h2>
                    <div class="resp-tab-content" aria-labelledby="tab_item-4">
                      <p>Концепция развития транслирует социометрический конкурент. Отсюда естественно следует, что анализ зарубежного опыта версифицирован. Партисипативное планирование поддерживает общественный продуктовый ассортимент. Медиа переворачивает принцип восприятия, учитывая результат предыдущих медиа-кампаний.</p>
                    </div>
                  </div>
                </div>
                <span class="clearfix"></span>
              </div>
            </div>
          </div>

        </div>

        <div class="row  section">
          <div id="system-message-container">
          </div>

        </div>

        <div class="row  section">
          <div class="col-md-12" style="margin-bottom:20px;">
            <div class="blog-featured" itemscope="" itemtype="https://schema.org/Blog">

            </div>

          </div>

        </div>

        <div class="row equal">

          <!-- text widget-->

          <div class="col-md-3">
            <div class="block text-center animate animated zoomIn" data-animation="zoomIn" style="height: 418px; visibility: visible; animation-delay: 0.5s;">
              <div class="block-inner">
                <div class="text-widget">
                  <div class="block-heading page-headers">
                    Важно знать! </div>

                  <div class="custom3">
                    <p>Воздействие на потребителя синхронизирует ролевой conversion rate, не считаясь с затратами. Повышение жизненных стандартов поддерживает продвигаемый формат события, работая над проектом. Визуализация концепии, как следует из вышесказанного, тормозит продукт. Клиентский спрос основан на опыте повседневного применения. Взаимодействие корпорации и клиента как всегда непредсказуемо.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <!-- Pricing table -->
          <div class="col-md-9">
            <div class="block animate animated lightSpeedIn" data-animation="lightSpeedIn" style="height: 418px; visibility: visible; animation-delay: 0.5s;">
              <div class="block-inner">
                <div class="block-heading">Цены и тарифы</div>
                <div class="row">

                  <div class="col-md-4">
                    <div class="price-box">
                      <div class="price-box-head">
                        <h4>СТАРТОВЫЙ</h4>
                        <div class="package-price">3 000 р\в мес.</div>
                      </div>
                      <ul>
                        <li>Комплекс услуг</li>

                        <li>Техническая поддержка</li>

                        <li>Скидки</li>

                        <li>Сертификаты</li>

                        <li>Ускоренная работа ЗАКАЗАТЬ</li>

                        <li><a href="#" class="btn btn-secondary btn-medium"><i class="fa fa-shopping-cart"></i> Заказать</a></li>
                      </ul>
                    </div>
                  </div>

                  <div class="col-md-4">
                    <div class="price-box">
                      <div class="price-box-head">
                        <h4>Стандарт</h4>
                        <div class="package-price">3 000 р\в мес.</div>
                      </div>
                      <ul>
                        <li>Комплекс услуг</li>

                        <li>Техническая поддержка</li>

                        <li>Скидки</li>

                        <li>Сертификаты</li>

                        <li>Ускоренная работа ЗАКАЗАТЬ</li>

                        <li><a href="#" class="btn btn-secondary btn-medium"><i class="fa fa-shopping-cart"></i> Заказать</a></li>
                      </ul>
                    </div>
                  </div>

                  <div class="col-md-4">
                    <div class="price-box">
                      <div class="price-box-head">
                        <h4>Максимальный</h4>
                        <div class="package-price">3 000 р\в мес.</div>
                      </div>
                      <ul>
                        <li>Комплекс услуг</li>

                        <li>Техническая поддержка</li>

                        <li>Скидки</li>

                        <li>Сертификаты</li>

                        <li>Ускоренная работа ЗАКАЗАТЬ</li>

                        <li><a href="#" class="btn btn-secondary btn-medium"><i class="fa fa-shopping-cart"></i> Заказать</a></li>
                      </ul>
                    </div>
                  </div>

                </div>
              </div>
            </div>
          </div>

        </div>


        <!-- Gallery -->
        <div class="row">

          <div class="col-md-12">
            <div class="block animate animated zoomInUp" data-animation="zoomInUp" style="visibility: visible; animation-delay: 0.5s;">
              <div class="block-inner">
                <div class="block-heading page-headers"><?=$label["gallery"];?></div>
                <?php echo do_shortcode('[ngg src="galleries" display="basic_thumbnail" thumbnail_crop="0"]');?>
              </div>
            </div>
          </div>

        </div>

        <div class="row equal">
          <!-- Our clients -->

          <div class="col-md-6">
            <div class="block animate animated rotateInUpLeft" data-animation="rotateInUpLeft" style="height: 226px; visibility: visible; animation-delay: 0.5s;">
              <div class="block-inner">

                <div class="block-heading"><?=$label["partners"];?></div>

                <div class="clients-slider carousel 6">

                  <div class="flex-viewport" style="overflow: hidden; position: relative;">
                    <ul class="slides" style="width: 1200%; transition-duration: 0s; transform: translate3d(-701px, 0px, 0px);">
                      <li class="banneritem" style="width: 200px; float: left; display: block;">
                        <img src="<?php echo get_template_directory_uri()?>/assets/image/partner/part1.jpg" alt="Партнер №1" draggable="false">
                        <div class="clr"></div>
                      </li>
                      <li class="banneritem" style="width: 200px; float: left; display: block;">
                        <img src="<?php echo get_template_directory_uri()?>/assets/image/partner/part2.jpg" alt="Партнер №2" draggable="false">
                        <div class="clr"></div>
                      </li>
                      <li class="banneritem" style="width: 200px; float: left; display: block;">
                        <img src="<?php echo get_template_directory_uri()?>/assets/image/partner/part3.jpg" alt="Партнер №3" draggable="false">
                        <div class="clr"></div>
                      </li>
                      <li class="banneritem" style="width: 200px; float: left; display: block;">
                        <img src="<?php echo get_template_directory_uri()?>/assets/image/partner/part4.jpg" alt="Партнер №4" draggable="false">
                        <div class="clr"></div>
                      </li>
                      <li class="banneritem" style="width: 200px; float: left; display: block;">
                        <img src="<?php echo get_template_directory_uri()?>/assets/image/partner/part5.jpg" alt="Партнер №5" draggable="false">
                        <div class="clr"></div>
                      </li>
                    </ul>
                  </div>
                  <ul class="flex-direction-nav">
                    <li><a class="flex-prev" href="#">Previous</a></li>
                    <li><a class="flex-next flex-disabled" href="#" tabindex="-1">Next</a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>

          <!-- Count to -->

          <div class="col-md-6">
            <div class="block bg-2 animate animated bounceInRight" data-animation="bounceInRight" style="height: 226px; visibility: visible; animation-delay: 0.5s;">
              <div class="block-inner">

                <div class="custom6">
                  <div class="row padding-tb30 text-center">
                    <div class="col-md-4">
                      <div class="bubble big-bubble bg-transblack">
                        <div class="live-count">456</div>
                        Клиентов</div>
                    </div>
                    <div class="col-md-4">
                      <div class="bubble big-bubble bg-transblack">
                        <div class="live-count">567</div>
                        Счетов</div>
                    </div>
                    <div class="col-md-4">
                      <div class="bubble big-bubble bg-transblack">
                        <div class="live-count">533</div>
                        Портфель</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>

        <div class="row equal">
          <!-- Contact widget -->

          <div class="col-md-3">
            <div class="block bg-3 animate animated bounceIn" data-animation="bounceIn" style="height: 462px; visibility: visible; animation-delay: 0.5s;">
              <div class="block-inner">
                <div class="block-heading page-headers"><?=$label["write_to_us"];?></div>

                <div class="custom3">
                  <form class="form-light" action="#">
                    <div class="field-wrapper">
                      <input name="name" required="" type="text" placeholder="<?=$label['name'];?>">
                      <div class="contactnameerror error-msg">Пожалуйста, введите имя</div>
                    </div>
                    <div class="field-wrapper">
                      <input name="phone" required="" type="text" placeholder="<?=$label['phone'];?>">
                      <div class="contactnameerror error-msg">Пожалуйста, введите email</div>
                    </div>
                    <div class="field-wrapper">
                      <textarea name="message" required="true" placeholder="<?=$label['message'];?>"></textarea>
                      <div class="contactnameerror error-msg">Пожалуйста, введите сообщение</div>
                    </div>
                    <div class="text-right"><span class="btn-send"> <i class="fa fa-paper-plane">&nbsp;</i><input class="btn-form-bot" type="submit" name="contact_form" value="<?=$label["send"];?>"></span></div>
                  </form>
                  <ul class="contact-details">
                    <li><i class="fa fa-phone">&nbsp;</i> (44) 630-20-23, (83422)4-42-62</li>
                    <li><i class="fa fa-envelope">&nbsp;</i> azizi_invest@mail.ru</li>
                    <li><i class="fa fa-map-marker">&nbsp;</i> Таджикистан, Худжанд,
                      <br> проспект И.Сомони, 203"б"
                      <br></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>

          <!-- Map -->

          <div class="col-md-9">
            <div class="block animate animated bounceIn" data-animation="bounceIn" style="height: 462px; visibility: visible; animation-delay: 0.5s;">
              <div class="block-inner" style="padding:0">
                <?php echo do_shortcode("[add_map]"); ?>
                <!-- <iframe src="<?php echo get_template_directory_uri(); ?>/assets/embed.php" width="100%" height="420" frameborder="0" style="border:0" allowfullscreen=""></iframe> -->
              </div>
            </div>
          </div>

        </div>

      </div>
      <!-- Middle Area -->
    </div>
    <!-- Container -->
  </div>
  <!-- Page Wrapper -->
  <?php get_footer(); ?>