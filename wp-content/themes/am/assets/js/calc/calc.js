var configForPercentage = {aSep: '', aDec: ',', mDec: 2, aPad: false, vMin: 0, vMax: 100}
var configForMoney = {aSep: ' ', aDec: ',', mDec: 2, vMin: 0, vMax: 10000000000, aPad:false}
var configForMoneyInt = {aSep: ' ', aDec: ',', mDec: 0, vMin: 0, vMax: 10000000000, aPad:false}

jQuery.validator.setDefaults({
    errorElement: 'div',
    errorClass: 'calc-error',
    onfocusout: false,
    errorPlacement:function(error, element){
        if (element.next().hasClass('chosen-container')) {
            element.next().after(error)
        } else {
            element.after(error)
        }
    },
    ignore: ":hidden:not(.chosen-select)"
})

jQuery.extend(jQuery.validator.messages, {
    required: 'Обязательное поле',
    min: 'Минимальное значение {0}',
    max: 'Максимальное значение {0}',
    number: "Введите число",
    step: 'Значение должно быть кратно {0}'
});



function number_format( number, decimals, dec_point, thousands_sep ) {
    var i, j, kw, kd, km;

    // input sanitation & defaults
    if( isNaN(decimals = Math.abs(decimals)) ){
        decimals = 2;
    }
    if( dec_point == undefined ){
        dec_point = ",";
    }
    if( thousands_sep == undefined ){
        thousands_sep = ".";
    }

    i = parseInt(number = (+number || 0).toFixed(decimals)) + "";

    if( (j = i.length) > 3 ){
        j = j % 3;
    } else{
        j = 0;
    }

    km = (j ? i.substr(0, j) + thousands_sep : "");
    kw = i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands_sep);
    //kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).slice(2) : "");
    kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).replace(/-/, 0).slice(2) : "");
    return km + kw + kd;
}


$('.calc-form.no-validation').on('submit', function() {
    $('input[type=submit]', this).addClass('disabled').prop('disabled', true)
})


$(document).ready(function() {

    $('.calc-tooltip').tooltip({animation: false, html: true})


    $('.calc-toggle').on('click', function() {

        var parent = $(this).parent()

        $('.calc-toggle', parent).removeClass('current')
        $(this).addClass('current')

        var name = $(this).data('name')
        var value = $(this).data('value')
        $('input[name='+name+']').val(value)

        $('.'+name+'-x').hide()
        $('.'+name+'-'+value).show()

        sendHeight()

        return false;
    })

    $('input[type=radio], .js-select-toggle').on('change', function() {

        var name = $(this).attr('name')
        var value = $(this).val()

        if ($('.'+name+'-x').length > 0) {
            $('.'+name+'-x').hide()
            $('.'+name+'-'+value).show()

            sendHeight()
        }
    })


    // сбрасываем ошибки у chosen
    $(document).on('change', 'select.chosen-select.calc-error', function() {
        if ($(this).val()) {
            $(this).removeClass('calc-error')
            var name = $(this).attr('name')
            $('#'+name+'-error').remove()
        }
    })

})

var sendHeight = function() {}