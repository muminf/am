

jQuery.validator.setDefaults({
    onfocusout: false,

    errorPlacement:function(error, element){
        if (element.next().hasClass('chosen-container')) {
            element.next().after(error)
        } else {
            element.after(error)
            //element.closest('.calc-fright').append(error)
        }
    },
    ignore: ":hidden:not(.chosen-select)",

    submitHandler: function (form) {
        var validator = this

        $('.calc-frow.result-row').hide();
        $('.calc-frow.loading-row').show();
        $('.calc-frow.result-sep').hide();

        $(':focus', '.calc-form').blur()


        $('.calc-loading').css('width', '0').show()
        var progress = 0;
        var progressInterval = setInterval(function () {
            progress+= 1;
            $('.calc-loading').css('width', progress+'%')
        }, 10)

        $('.calc-submit', form).prop('disabled', true)
        sendHeight()

        var formData = $(form).serialize()

        if (typeof isEmbed !== 'undefined') {
            formData+= '&isEmbed=1';
        }
        formData+= '&referer='+encodeURIComponent(document.referrer)

        $.ajax({
            type: 'post',
            url: $(form).attr('action'),
            dataType: 'json',
            data: formData,
            success: function (result) {

                $('.calc-frow.loading-row').hide();
                $('.calc-frow.result-sep').show();
                $('.calc-loading').css('width', '100%')
                $('.calc-loading').hide()

                clearInterval(progressInterval)

                for (var name in result) {
                    $('.result-placeholder-'+name).html(result[name]).closest('.result-row').show()
                }
                $('.calc-submit', form).prop('disabled', false)

                $('.calc-panel-item.result-only').css('display', 'inline-block')
                if ('increase_counter' in result && $('.today-count-placeholder').length > 0) {
                    var counter = Number($('.today-count-placeholder').text())
                    $('.today-count-placeholder').text(++counter)
                }

                if (typeof onCalculate == 'function') {
                    onCalculate()
                }
                //$('input[name=result_name]', '#saveResultModal').val(result.suggested_name)

                sendHeight()

            },
            error: function() {
                $('.calc-frow.result-row').hide();
                $('.calc-frow.loading-row').hide();
                alert('Service is temporary unavailable.')
                $('input[type=submit]', form).prop('disabled', false)
                $('.calc-panel-item.result-only').css('display', 'none !important')

                sendHeight()
            }
        })
        return false
    },

    invalidHandler: function() {
        setTimeout(sendHeight, 1)
    },

    unhighlight: function(element, errorClass, validClass) {
        //jQuery.validator.defaults.unhighlight.call(element, errorClass, validClass)
        if ( element.type === "radio" ) {
            this.findByName( element.name ).removeClass( errorClass ).addClass( validClass );
        } else {
            $( element ).removeClass( errorClass ).addClass( validClass );
        }
        setTimeout(sendHeight,1)
    },

    highlight: function( element, errorClass, validClass ) {
        if ( element.type === "radio" ) {
            this.findByName( element.name ).addClass( errorClass ).removeClass( validClass );
        } else {
            $( element ).addClass( errorClass ).removeClass( validClass );
        }
        setTimeout(sendHeight,1)
    }
})

$(document).ready(function () {
    $('.calc-form').validate()

})

