<?php

add_action('wp_enqueue_scripts', 'style_theme');
add_action('wp_enqueue_scripts', 'scripts_theme');
add_action('after_setup_theme', 'main_menu');
add_theme_support( 'post-thumbnails' );
add_image_size( 'preview', 200, 200, true );

function style_theme() {
    $uri = get_template_directory_uri();
    wp_enqueue_style('style', get_stylesheet_uri());
    wp_enqueue_style('simple-line-icons.min.css', $uri . '/assets/css/simple-line-icons.min.css');
    wp_enqueue_style('k2.css', $uri . '/assets/css/k2.css');
    wp_enqueue_style('bootstrap.css', $uri . '/assets/css/bootstrap.css');
    wp_enqueue_style('animate.css', $uri . '/assets/css/animate.css');
    wp_enqueue_style('font-awesome.min.css', $uri . '/assets/css/font-awesome.min.css');
    wp_enqueue_style('style.css', $uri . '/assets/css/style.css');
    wp_enqueue_style('theme.css', $uri . '/assets/css/theme.css');
    wp_enqueue_style('css.css', $uri . '/assets/css/css.css');
}

function scripts_theme() {
    $uri = get_template_directory_uri();
    //wp_enqueue_script('watch.js', $uri . '/assets/js/watch.js');
    wp_enqueue_script('k2.frontend.js', $uri . '/assets/js/k2.frontend.js');
    wp_enqueue_script('jquery.min.js', $uri . '/assets/js/jquery.min.js');
    wp_enqueue_script('jquery-noconflict.js', $uri . '/assets/js/jquery-noconflict.js');
    wp_enqueue_script('jquery-migrate.min.js', $uri . '/assets/js/jquery-migrate.min.js');
    wp_enqueue_script('caption.js', $uri . '/assets/js/caption.js');
    wp_enqueue_script('newCapt.js', $uri . '/assets/js/newCapt.js');
   
    wp_enqueue_script('modernizr.js', $uri . '/assets/js/modernizr.js');
    wp_enqueue_script('jquery-1.11.1.min.js', $uri . '/assets/js/jquery-1.11.1.min.js');
    wp_enqueue_script('jquery.flexslider.js', $uri . '/assets/js/jquery.flexslider.js');
    wp_enqueue_script('js(1).js', $uri . '/assets/js/js(1).js');
    wp_enqueue_script('plugins.js', $uri . '/assets/js/plugins.js');
    wp_enqueue_script('scripts.js', $uri . '/assets/js/scripts.js');
    wp_enqueue_script('common.js', $uri . '/assets/js/common.js');
    wp_enqueue_script('util.js', $uri . '/assets/js/util.js');


    wp_enqueue_script('init_embed.js', $uri . '/assets/js/init_embed.js');
    wp_enqueue_script('map.js', $uri . '/assets/js/map.js');
    wp_enqueue_script('overlay.js', $uri . '/assets/js/overlay.js');
	
}

function main_menu() {
    register_nav_menu('main', 'Главное меню');
}

function get_rate($nbt=false) {
    global $wpdb;
    $sql = "SELECT cr.id,cr.char_code AS char_code, cr.name AS NAME, cr.nominal AS nominal, rt.buy AS buy, rt.buy_fiz AS buy_fiz, rt.sell AS sell, rt.sell_fiz AS sell_fiz, rt.datetime AS DATE 
            FROM currency AS cr, currency_rate".($nbt>0 ? "_nbt" : "")." AS rt 
            WHERE rt.currency IN(2,3,4) AND rt.currency=cr.id GROUP BY rt.`currency`,rt.datetime ORDER BY rt.id DESC,DATETIME DESC LIMIT 3";
    $result = $wpdb->get_results($sql);
    return $result;   
}

$key = "qwerty";
function getToken($data){
    global $key;
    return sha1(md5(json_encode($data)).$key);
}

function load_rate($data, $nbt=0) {
    global $wpdb;
    $rates = $data["rate"];
    $format = array('%d','%f','%f','%s');
    $table = (intval($nbt)>0 ? "currency_rate_nbt" : "currency_rate");
    foreach($rates as $k=>$v){
        $rates[$k]["datetime"] = date("Y-m-d H:i:s");
        $wpdb->insert($table,$rates[$k],$format);
    }
}

function get_submenu($css_class="") {
    $result = array();
    $result["list"] = "";
    $id = "";
    if ( $menu_items = wp_get_nav_menu_items(2) ) {  //var_dump($menu_items);
        foreach ( $menu_items as $menu_item ) {
            if($menu_item->object_id == get_queried_object_id()){
                $result["title"] = $menu_item->title;
                $id = $menu_item->post_name;
            }
           
            if($menu_item->menu_item_parent ==  $id){
                $current = ( $menu_item->object_id == get_queried_object_id() ) ? $css_class : '';
                $result["list"] .= '<li class="' . $current . '"><a href="' . $menu_item->url . '">' . $menu_item->title . '</a></li>';
            }
        }
    }
    return $result;
}

add_shortcode('cred_calc', 'add_cred_calc');

function add_cred_calc() {
    global $lang;
    return '
    <iframe id="calcus-iframe-credit" width="100%" height="300" frameborder="0" style="width:1px; min-width:100%;" scrolling="no" src="'.get_site_url().'/calc/index.php?lang='.$lang.'"></iframe>
        
        <script>
            window.addEventListener(\'message\', function(event) {
                if(height = event.data[\'height\']) {
                    document.getElementById(\'calcus-iframe-credit\').style.height =  height + 50 + \'px\'
                }
            })
        </script>';
}

add_shortcode('contact_form', 'contact_form');
function add_contact_form() {
    return '<div class="block bg-3 animate animated bounceIn" data-animation="bounceIn" style="height: 462px; visibility: visible; animation-delay: 0.5s;">
			  <div class="block-inner">
			    <div class="block-heading page-headers">
			      Написать нам </div>

			    <div class="custom3">
			      <form action="/#wpcf7-f186-o1" method="post" class="wpcf7-form" novalidate="novalidate">
			        <div style="display: none;">
			          <input type="hidden" name="_wpcf7" value="186">
			          <input type="hidden" name="_wpcf7_version" value="5.1.6">
			          <input type="hidden" name="_wpcf7_locale" value="ru_RU">
			          <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f186-o1">
			          <input type="hidden" name="_wpcf7_container_post" value="0">
			        </div>
			        <div class="field-wrapper">
			          <input name="your-name" required="" type="text" placeholder="Имя">
			          <div class="contactnameerror error-msg">Пожалуйста, введите имя</div>
			        </div>
			        <div class="field-wrapper">
			          <input name="your-email" required="" type="email" placeholder="Email">
			          <div class="contactnameerror error-msg">Пожалуйста, введите email</div>
			        </div>
			        <div class="field-wrapper">
			          <textarea name="your-message" required="" placeholder="Сообщение"></textarea>
			          <div class="contactnameerror error-msg">Пожалуйста, введите сообщение</div>
			        </div>
			        <div class="text-right"><span class="btn-send"> <i class="fa fa-paper-plane">&nbsp;</i><input class="btn-form-bot" type="submit" value="Готово"></span></div>
			      </form>
			      <ul class="contact-details">
			        <li><i class="fa fa-phone">&nbsp;</i> (44) 630-20-23, (83422)4-42-62</li>
			        <li><i class="fa fa-envelope">&nbsp;</i> azizi_invest@mail.ru</li>
			        <li><i class="fa fa-map-marker">&nbsp;</i> Таджикистан, Худжанд,
			          <br> проспект И.Сомони, 203"б"
			          <br></li>
			      </ul>
			    </div>
			  </div>
			</div>';
}
add_shortcode('add_map', 'add_map');

function add_map() {
    return '<iframe src="https://www.google.com/maps/d/embed?mid=1fUT1aYMxP286f6XJpYkKrawjCcZA-ZYQ" width="100%" height="420"> </iframe>';
}

function send_telegram($chat_id,$token,$data){
	$txt = "";
	$fields = array(
		'city' => 'Город',
		'name' => 'Имя',
		'phone' => 'Телефон',
		'message' => 'Сообщение',
		'summa' => 'Сумма',
		'valuta' => 'Валюта'
	);
	foreach($data as $k=>$v){
		$txt .= (array_key_exists($k, $fields) ? '<b>'.$fields[$k].':</b> '.strip_tags($v)."%0A" : '<b>'.$k.':</b> '.strip_tags($v)."%0A");
	}
	
	$sendToTelegram = file_get_contents("https://api.telegram.org/bot{$token}/sendMessage?chat_id={$chat_id}&parse_mode=html&text={$txt}");
	return $sendToTelegram;
}

add_action('init', 'taxonomy_type');
function taxonomy_type(){

	// список параметров: wp-kama.ru/function/get_taxonomy_labels
	register_taxonomy( 'taxonomy', [ 'page','post' ], [ 
		'label'                 => '', // определяется параметром $labels->name
		'labels'                => [
			'name'              => 'Тип страницы',
			'singular_name'     => 'Тип',
			'search_items'      => 'Искать типы',
			'all_items'         => 'All Genres',
			'view_item '        => 'View Genre',
			'parent_item'       => 'Parent Genre',
			'parent_item_colon' => 'Parent Genre:',
			'edit_item'         => 'Edit Genre',
			'update_item'       => 'Update Genre',
			'add_new_item'      => 'Add New Genre',
			'new_item_name'     => 'New Genre Name',
			'menu_name'         => 'Тип страницы',
		],
		'description'           => '', // описание таксономии
		'public'                => true,
		// 'publicly_queryable'    => null, // равен аргументу public
		// 'show_in_nav_menus'     => true, // равен аргументу public
		// 'show_ui'               => true, // равен аргументу public
		// 'show_in_menu'          => true, // равен аргументу show_ui
		// 'show_tagcloud'         => true, // равен аргументу show_ui
		// 'show_in_quick_edit'    => null, // равен аргументу show_ui
		'hierarchical'          => false,

		'rewrite'               => true,
		//'query_var'             => $taxonomy, // название параметра запроса
		'capabilities'          => array(),
		'meta_box_cb'           => null, // html метабокса. callback: `post_categories_meta_box` или `post_tags_meta_box`. false — метабокс отключен.
		'show_admin_column'     => false, // авто-создание колонки таксы в таблице ассоциированного типа записи. (с версии 3.5)
		'show_in_rest'          => null, // добавить в REST API
		'rest_base'             => null, // $taxonomy
		// '_builtin'              => false,
		//'update_count_callback' => '_update_post_term_count',
	] );
}

$lang = substr(get_bloginfo("language"),0,2);
$lang = (in_array($lang, array("en","tj")) ? strtolower($lang) : "ru");
$labels = array(
    "ru" => array("news" => "Новости",
                  "about" => "О КОМПАНИИ",
                  "gallery" => "Фото галерея",
                  "partners" => "НАШИ ПАРТНЕРЫ",
                  "write_to_us" => "Написать нам",
                  "name" => "Имя",
                  "phone" => "Телефон",
                  "message" => "Сообщение",
                  "send" => "Отправить",
                  "currency_rate" => "Курс валют",
                  "nbt" => "НБТ",
                  "buy" => "Покупка",
                  "sell" => "Продажа",
                  "currency" => array(
                    2 => "Доллар США",
                    3 => "Евро",
                    4 => "Российский рубль"
                  ),
                  "month_name" => array(1 => "Января",2 => "Февраля",3 => "Марта",4 => "Апреля",5 => "Мая",6 => "Июня",7 => "Июля",8 => "Августа",9 => "Сентября",10 => "Октября",11 => "Ноября",12 => "Декабря")
            ),
    "tj" => array("news" => "Хабархо",
                  "about" => "Дар бораи мо",
                  "gallery" => "Галереяи расмхо",
                  "partners" => "Хамкори бо мо",
                  "write_to_us" => "Алока бо мо",
                  "name" => "Ному насаб",
                  "phone" => "Телефон",
                  "message" => "Нома",
                  "send" => "Ирсол",
                  "currency_rate" => "Курби асъор",
                  "nbt" => "БМТ",
                  "buy" => "Харид",
                  "sell" => "Фуруш",
                  "currency" => array(
                    2 => "Долларм ИМА",
                    3 => "Евро",
                    4 => "Рубли руссия"
                  ),
                  "month_name" => array(1 => "Январи",2 => "Феврали",3 => "Марти",4 => "Апрели",5 => "Майи",6 => "Июни",7 => "Июли",8 => "Августи",9 => "Сентябри",10 => "Октябри",11 => "Ноябри",12 => "Декабри")
            ),
    "en" => array("news" => "News",
                  "about" => "About",
                  "gallery" => "Gallery",
                  "partners" => "Partners",
                  "write_to_us" => "Contacts",
                  "name" => "Name",
                  "phone" => "Phone",
                  "message" => "Message",
                  "send" => "Send",
                  "currency_rate" => "Currency rate",
                  "nbt" => "NBT",
                  "buy" => "Buy",
                  "sell" => "Sell",
                  "currency" => array(
                     2 => "USA Dollars",
                     3 => "Euro",
                     4 => "Russian rubles"
                  ),
                  "month_name" => array(1 => "January",2 => "Febrary",3 => "March",4 => "April",5 => "May",6 => "June",7 => "July",8 => "August",9 => "September",10 => "October",11 => "November",12 => "December")
            )
);
$label = $labels[$lang];