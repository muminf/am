<?php get_header('page'); ?>

<!-- Page Title
   ================================================== -->
   <div id="page-title">

      <div class="row">
<?php
get_post();
$pagename = get_query_var('pagename');  
$page = get_page_by_path( $pagename );
get_the_title( $page );
the_post();
?>
         

      </div>

   </div> <!-- Page Title End-->

   <div class="middle" style="padding-bottom: 239px;">
  <!-- Icon holder -->

      <div class="row equal">
         <!-- Icon box -->

         <!-- Tab -->
      </div>

      <!-- Tab -->

      <div class="row  section">
         <div id="system-message-container">
         </div>

      </div>

      <div class="row  section">

      <div class="col-md-3">

         <!--<div class="block animate animated fadeIn" data-animation="fadeIn" style="visibility: visible; animation-delay: 0.5s;">

         <div class="block-inner ">
            <div class="search">
               <form action="/index.php/contact" method="post" class="form-inline">
               <button class="button btn btn-primary" onclick="this.form.searchword.focus();">ОК</button>
               <label for="mod-search-searchword201" class="element-invisible">Искать...</label>
               <input name="searchword" id="mod-search-searchword201" maxlength="200" class="inputbox search-query input-medium" type="search" placeholder="Поиск...">
               <input type="hidden" name="task" value="search">
               <input type="hidden" name="option" value="com_search">
               <input type="hidden" name="Itemid" value="111">
               </form>
            </div>
         </div> 

         </div>-->

         <div class="block animate animated fadeIn" data-animation="fadeIn" style="visibility: visible; animation-delay: 0.5s;">

         <div class="block-inner ">
            <?php $submenu = get_submenu("current");?>
            <div class="block-heading page-headers"><?=$page->title;?></div>
               <ul class="nav menu">
                  <?=$submenu["list"]?>
               </ul>
            <div class="bannergroup">

               <div class="banneritem">
               <a href="/index.php/component/banners/click/1" target="_blank" rel="noopener noreferrer" title="На правах рекламы">
                  <img src="http://businesses.html6.us/images/banners/gallery-thumb-2.jpg" alt="На правах рекламы">
               </a>
               <div class="clr"></div>
               </div>

               <div class="bannerfooter">
               Маркетингово-ориентированное издание слабо изменяет повторный контакт. Согласно последним исследованиям, процесс стратегического планирования отражает медиамикс. Стимулирование сбыта определяет типичный conversion rate. Стимулирование сбыта ускоряет пресс-клиппинг. Sed non mauris vitae erat consequat auctor eu in elit. Sed non neque elit. </div>
            </div> 
         </div>

         </div>

         </div>

         <div class="col-md-9" style="margin-bottom:20px;">
            <div class="content" itemscope="" itemtype="https://schema.org/Person">

            <div class="page-header">
               <h2>
                  <span class="contact-name" itemprop="name"><?php the_title();?></span>
               </h2>
            </div>
            <?php the_content(); ?>
            </div>

         </div>
         
         
         
      </div>

      <!-- Team -->

      <!-- Gallery -->

      </div>
 

<?php get_footer(); ?>