<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('WP_CACHE', true);
define( 'WPCACHEHOME', 'C:\xampp\htdocs\am\wp-content\plugins\wp-super-cache/' );
define( 'DB_NAME', 'am' );

/** Имя пользователя MySQL */
define( 'DB_USER', 'root' ); 

/** Пароль к базе данных MySQL */
define( 'DB_PASSWORD', '' );

/** Имя сервера MySQL */
define( 'DB_HOST', '' );

/** Кодировка базы данных для создания таблиц. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Схема сопоставления. Не меняйте, если не уверены. */
define( 'DB_COLLATE', '' );

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'kle_At,/F_RG#mL=Rb-Mb7X2=^e|J06!3J0pACAq9R^9E|k WRNJ_>5h,XlVN$T3' );
define( 'SECURE_AUTH_KEY',  'S`!e-*`5g(f7x8V<$,3ZDT7y<5e)Z4=zF qp`)JCPxgTgHeU+:E#g&;;nB=3Ll_J' );
define( 'LOGGED_IN_KEY',    'MoF!MxNa8T;T?/0A7~&HY3D~/[sPx#]Br0VF:v~9!phS&9zl9h)0F>[X4x/a^ori' );
define( 'NONCE_KEY',        '0Es}8a#R7gjnL;g8DCl!@2eNf)yNY(YLSb;kgB5KZJvg3:i2LqNPX./S!.@]^&Cb' );
define( 'AUTH_SALT',        '+Iv<MXb4+syrC$<su0Q]38av;6/FdF&maith4fH1py4sNkUk&]kH9_%{:9ew9YPM' );
define( 'SECURE_AUTH_SALT', '8U?BvjC>fjvGVz/*kef(,WT;q=R&PeBJC;w532aF2YMfPu*aQ1lNtWyl7NZts)Nx' );
define( 'LOGGED_IN_SALT',   '29MdY11H-aNtzA,5qA-Z ^Us:fA1W.L1JiP*!0~fG67WfqOu l_HS[h %A74$;w`' );
define( 'NONCE_SALT',       '|31S0dHuCz*6k,dq~&*n<^mhs*W(>),LLOo_uX(4yDkmS0@3i`qy.,>>Fv[`Cm/?' );

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix = 'am_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Инициализирует переменные WordPress и подключает файлы. */
require_once( ABSPATH . 'wp-settings.php' );
